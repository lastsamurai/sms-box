<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190508114451 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, number VARCHAR(20) NOT NULL, name VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sms (id INT AUTO_INCREMENT NOT NULL, text VARCHAR(160) NOT NULL, created_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sms_contact (sms_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_BC782DE2BD5C7E60 (sms_id), INDEX IDX_BC782DE2E7A1254A (contact_id), PRIMARY KEY(sms_id, contact_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sms_contact ADD CONSTRAINT FK_BC782DE2BD5C7E60 FOREIGN KEY (sms_id) REFERENCES sms (id)');
        $this->addSql('ALTER TABLE sms_contact ADD CONSTRAINT FK_BC782DE2E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sms_contact DROP FOREIGN KEY FK_BC782DE2E7A1254A');
        $this->addSql('ALTER TABLE sms_contact DROP FOREIGN KEY FK_BC782DE2BD5C7E60');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE sms');
        $this->addSql('DROP TABLE sms_contact');
    }
}
