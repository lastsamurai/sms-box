<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190508124304 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE sms_contact DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE sms_contact ADD id INT AUTO_INCREMENT NOT NULL, ADD status INT NOT NULL, ADD timestamp DATETIME NOT NULL, ADD api VARCHAR(255) NOT NULL, ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sms_contact MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE sms_contact DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE sms_contact DROP id, DROP status, DROP timestamp, DROP api');
    }
}
