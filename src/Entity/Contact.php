<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Carbon\Carbon;
use App\Entity\SmsContact;
/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 * @ORM\Table(name="contact")
 */
class Contact
{
    /**
     * @var \Doctrine\Common\Collections\Collection|Sms[]
     *
     * @ORM\OneToMany(targetEntity="SmsContact", mappedBy="contact")
     */
     
    protected $smscontacts;
    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
        $this->smscontacts = new ArrayCollection();
        $this->created_at=Carbon::now();
    }

    public function delete(){
        $this->deleted_at=Carbon::now();
    }

    /**
     * @param SmsContact $smscontact
     */
    public function addSmsContact(SmsContact $smscontact)
    {
        if ($this->smscontacts->contains($smscontact)) {
            return;
        }
        $this->smscontacts->add($smscontact);
        $smscontact->addContact($this);
    }
    /**
     * @param Sms $sms
     */
    public function removeSmsContact(Sms $smscontact)
    {
        if (!$this->smscontacts->contains($smscontact)) {
            return;
        }
        $this->smscontacts->removeElement($smscontact);
        $smscontact->removeContact($this);
    }
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
