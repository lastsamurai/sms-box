<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Carbon\Carbon;
/**
 * @ORM\Entity(repositoryClass="App\Repository\SmsRepository")
 * @ORM\Table(name="sms")
 */
class Sms
{

     /**
     * @ORM\OneToMany(targetEntity="SmsContact", mappedBy="sms", fetch="EXTRA_LAZY")
     */
    protected $smscontacts;
    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
        $this->smscontacts = new ArrayCollection();
        $this->created_at=Carbon::now();
    }

    public function delete(){
        $this->deleted_at=Carbon::now();
    }

    /**
     * @param SmsContact $smscontact
     */
    public function addSmsContact(SmsContact $smscontact)
    {
        if ($this->smscontacts->contains($smscontact)) {
            return;
        }
        $this->smscontacts->add($smscontact);
        $smscontact->addSms($this);
    }
    /**
     * @param SmsContact $smscontact
     */
    public function removeSmsContact(SmsContact $smscontact)
    {
        if (!$this->smscontacts->contains($smscontact)) {
            return;
        }
        $this->smscontacts->removeElement($smscontact);
        $smscontact->removeSms($this);
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=160)
     */
    private $text;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
}
