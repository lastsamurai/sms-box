<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Carbon\Carbon;
use App\Controller\ApiController;

/**
 * @ORM\Entity
 * @ORM\Table(name="sms_contact")
 */

class SmsContact{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    public $id;

    public function __construct()
    {
        $this->timestamp=Carbon::now();
        $this->status=0;
        $this->api=ApiController::$API_list[1];
    }

    /**
     * @ORM\ManyToOne(targetEntity="Sms", inversedBy="smsContact")
     * @ORM\JoinColumn(nullable=false)
     */

    public $sms;

    /**
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="smsContact")
     * @ORM\JoinColumn(nullable=false)
     */
    public $contact;

    /**
     * @ORM\Column(type="integer")
     */
    public $status;
    /**
     * @ORM\Column(type="datetime")
     */
    public $timestamp;
    /**
     * @ORM\Column(type="string")
     */
    public $api;

    public function sendToApi(){
        
    }
}



?>