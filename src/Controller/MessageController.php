<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Sms;
use Carbon\Carbon;

class MessageController extends AbstractController
{
    
    /**
     * @Route("/message", name="message")
     */
    public function index()
    {      
        return $this->showAll();
    }

    public function getAll(){
        $ss=$this->getDoctrine()->getRepository(Sms::class)->findAll();
        $newArr=array_filter($ss,function($s){
            if($s->getDeletedAt()==null)return true;
            return false;
        });
        $newArr=array_map(function($s){
            return [
                "id"=> $s->getId(),
                "digest"=> substr($s->getText(),0,14)."...",
                "created_at"=>$s->getCreatedAt()->format('Y-m-d h:m:s')
            ];
        },$newArr);
        return $newArr;
    }

    private function ValidationMiddleware($id){   
        if($id<0) return false;
        $s=$this->getDoctrine()->getRepository(Sms::class)->find($id);
        if(!$s)  return false;
        return true;
    }

    public function show($id){
        $newArr=$this->getAll();

        $s=$this->getDoctrine()->getRepository(Sms::class)->find($id);
        if(!$s) throw new \Exception('No Sms found for id '.$id);
        $message= [
            "body"=>$s->getText(),
            "id" => $id,
            "digest"=> substr($s->getText(),0,14)."..."
        ];

        $data=[
            "messages_n" => count($newArr),
            "messages" =>$newArr,
            "message" => $message
        ];
        return $this->render('messages.html.twig',$data);
    }

    public function showAll(){
        $newArr=$this->getAll();
        $data=[
            "messages_n" => count($newArr),
            "messages" =>$newArr
        ];
        return $this->render('messages.html.twig',$data);
    }



    public function create(Request $req){
        $s=new Sms();
        $body=$req->request->get('body');

        if(!$body) throw new \Exception('text message is empty');

        $s->setText( $body );

        $em=$this->getDoctrine()->getEntityManager();
        $em->persist($s);
        $em->flush();
        $id=$s->getId();
        
        if($req->request->get('sendFlag')=="create&send")
            return $this->redirect($this->generateURL('sendMessage',['id'=>$id]));
        return $this->show($id);
    }

    public function update(Request $req,$id){
        $body=$req->request->get('body');
        if(!$body) throw new \Exception('text message is empty');

        $s=$this->getDoctrine()->getRepository(Sms::class)->find($id);
        if(!$s) throw new \Exception('No Sms found for id '.$id);

        $s->setText($body);

        $em=$this->getDoctrine()->getEntityManager();
        $em->persist($s);
        $em->flush();

        return $this->show($id);
    }
    public function delete($id){
        $s=$this->getDoctrine()->getRepository(Sms::class)->find($id);
        
        if($s==null) $this->redirect($this->generateUrl('index'));

        $s->delete();
        
        $em=$this->getDoctrine()->getEntityManager();
        $em->persist($s);
        $em->flush();
        
        return $this->redirect($this->generateUrl('index'));
    }

}
