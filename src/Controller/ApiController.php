<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\SmsContact;
use App\Entity\Sms;
use App\Entity\Contact;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;


class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */

     public static $API_list=[
        1=>'http://localhost:8001/',
        2=>'http://localhost:8002/'
    ];
     private static $messages=[];

    
    public function index()
    {
        
        return $this->render('report.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    public function sendMessageToContact($sid,$cid){

        
        $sms=$this->getDoctrine()->getRepository(Sms::class)->find($sid);
        if(!$sms)throw new \Exception("No Message Specified to Send");
        $contact=$this->getDoctrine()->getRepository(Contact::class)->find($cid);
        if(!$contact)throw new \Exception("No Contact Specified to Send");
        
        $smscontact=new SmsContact();
        $smscontact->sms=$sms;
        $smscontact->contact=$contact;

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($smscontact);
        $entityManager->persist($contact);
        $entityManager->persist($sms);
        $entityManager->flush();

        $res=$this->send($smscontact,ApiController::$API_list[1]);
        if($res==404){
            $res=$this->send($smscontact,ApiController::$API_list[2]);
            if($res==404){
                $this->push_message($smscontact);
            }
        }

        return $this->redirect($this->generateUrl('report'));
    }


    public function push_message(SmsContact $s){
        ApiController::$messages[]=$s;
    }

    private function send(SmsContact &$sms,$api){
        
        if(!$sms) throw new \Exception("Message Not valid to send");

        $sms->api= $api;

        $sms->status=0;//NOT SENT YET

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($sms);
        
        $client = new \GuzzleHttp\Client();

        $url= $api.
                "?number=".urlencode($sms->contact->getNumber()).
                "&body=".urlencode($sms->sms->getText());
        
        try{
            $response=$client->request('GET',$url);
        }catch(\Exception $e){
            return 404;
        }
        
        $status=$response->getStatusCode(); # 200

        if($status==200){
            $sms->status=1;//successfully SENT
            
            $entityManager->persist($sms);
            $entityManager->flush();
            return 200;
        }

        $sms->status=2;//FAILED
        $entityManager->persist($sms);
        $entityManager->flush();
        
        return 404;
    }
}
