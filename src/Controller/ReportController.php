<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\SmsContact;
use App\Controller\ApiController;

class ReportController extends AbstractController
{
    /**
     * @Route("/report", name="report")
     */
    public function index(Request $req)
    {   
                
        $smscontacts=$this->getDoctrine()->getRepository(SmsContact::class)->findAll();
        $arr=array_map(function($v){
            return [
                "number"=> $v->contact->getNumber(),
                "message"=> $v->sms->getText(),
                "status" => $v->status,
                "api" => $v->api
            ];
        },$smscontacts);
        $newArr=[];
        $query=$req->query->get('query');
        if($query){
            foreach($arr as $v)
                if(strpos($v['number'],$query))
                    $newArr[]=$v;
        }
        $a= ($query)?$newArr:$arr;
        



        $em = $this->getDoctrine()->getManager();

        $RAW_QUERY = 'SELECT * , COUNT( * ) AS count FROM sms_contact GROUP BY contact_id ORDER BY id DESC LIMIT 0 , 30';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $result = $statement->fetchAll();

        $recipients_n=count($result);


        $RAW_QUERY = 'SELECT id,name,number FROM contact NATURAL JOIN ( SELECT contact_id AS id FROM sms_contact GROUP BY contact_id ORDER BY id DESC LIMIT 0 , 30)AS T WHERE 1;';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $contacts = $statement->fetchAll();

        $RAW_QUERY = 'SELECT id FROM sms_contact ;';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $N = count($statement->fetchAll());
        
        $RAW_QUERY = 'SELECT id FROM sms_contact WHERE api = "'.ApiController::$API_list[1].'";';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $api1 = count($statement->fetchAll());
        
        $RAW_QUERY = 'SELECT id FROM sms_contact WHERE api = "'.ApiController::$API_list[2].'";';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $api2 = count($statement->fetchAll());
        
        $perc1=$api1 * 100 / ($N!=0?$N:1);
        $perc2=$api2 * 100 / ($N!=0?$N:1);


        $RAW_QUERY = 'SELECT id FROM sms_contact WHERE api = "'.ApiController::$API_list[1].'" and status=0;';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $rate1 = count($statement->fetchAll())*100 / ($api1!=0?$api1:1);

        $RAW_QUERY = 'SELECT id FROM sms_contact WHERE api = "'.ApiController::$API_list[2].'" and status=0;';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $rate2 = count($statement->fetchAll())*100 / ($api2!=0?$api2:1);

        
        $data=[
            "sent_messages"=> $a,
            "total"=>count($a),
            "recipients_n"=>$recipients_n,
            "contacts"=>$contacts,
            "api1"=> ApiController::$API_list[1],
            "api2"=> ApiController::$API_list[2],
            "api1_perc"=> $perc1,
            "api2_perc"=> $perc2,
            "rate1"=>$rate1,
            "rate2"=>$rate2,
        ];
        if($query)
            $data["query"]=$query;
        
        //return new Response(dd($data));

        return $this->render('report.html.twig', $data);
    }
}
