<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\MessageController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Contact;
use App\Entity\Sms;
use App\Entity\SmsContact;
use Carbon\Carbon;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index()
    {
        
    }

    public function getAll(){
        $ss=$this->getDoctrine()->getRepository(Contact::class)->findAll();
        $newArr=array_filter($ss,function($s){
            if($s->getDeletedAt()==null)return true;
            return false;
        });
        $newArr=array_map(function($s){
            return [
                "id"=> $s->getId(),
                "name"=>$s->getName(),
                "number"=>$s->getNumber(),
                "deleted_at"=> $s->getDeletedAt()?$s->getDeletedAt()->format('Y-m-d h:m:s'):null, 
                "created_at"=>$s->getCreatedAt()->format('Y-m-d h:m:s')
            ];
        },$newArr);
        return $newArr;
    }

    public function showAll(){
        $newArr=$this->getAll();
        
        $data=[
            "contact_n" => count($newArr),
            "contacts" =>$newArr
        ];
        
        return $this->render('contacts.html.twig',$data);
    }

    public function create(Request $req){
        //middleware to validate input
        $name=$req->request->get('name');
        $num=$req->request->get('number');
        
        if(!$name || !$num) throw new \Exception("No Contact Info to Register");
        if(!is_numeric($num)) throw new \Exception("Invalid Number as input");

        $c=new Contact();
        $c->setName($name);
        $c->setNumber($num);
        $c->setCreatedAt(Carbon::now());

        $em=$this->getDoctrine()->getEntityManager();
        $em->persist($c);
        $em->flush();

        return $this->redirect($this->generateUrl('contact'));
    }

    public function sendMessage($id){
        $ss=$this->getDoctrine()->getRepository(Sms::class)->find($id);
        if(!$ss) throw new \Exception("No Message Specified to Send");
        $c=$this->getAll();

        $data=[
            'message'=>[
                "body"=>$ss->getText(),
                "id"=>$id
            ],
            'contacts'=>$c
        ];
        
        return $this->render('sendToContact.html.twig',$data);
    }

    
}
