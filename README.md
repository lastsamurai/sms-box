# SMS Box

SMS-Box is a Symfony application where you can send text messages to contacts via external APIs.

## Installation

Everything should be easy to set up by cloning the publicly available [Gitlab](https://gitlab.com/lastsamurai/sms-box) repository

Please ensure You run the following:

```bash
#update composer within the project folder
composer update

#set DATABASE_URL enviroment variable within .env file
vim .env

#create a Mysql/MariaDB database compliant with your DATABASE_URL
#and then run the migrations
php bin/console migrate
```

## How it works
The User Interface is intuitive enough, however some things worth mentioning are:
* External APIs to which the messages are sent are declared within **ApiController** as the static
array **ApiController::$API_list** so **please make sure you specify your own API URLs**
* To Create/Read/Update/Delete messages, visit **/message** route (or simply the home page)
* Before you send a message, You need to create a contact by visiting the **/contact** route (or simply clicking on _contacts_ tab).
Being that this is a minimal application, you cannot delete or update contacts
* To Send Messages, start with the homepage where you can select/create a message.
Next, you’ll be taken to the contact page where you’ll pick an already registered Contact
& Finally the message will be sent to the external APIs and a report page will be displayed

## Technical Notes
* **Exceptions** are thrown when unexpected or meaningless actions are taken such as _sending an empty message_ or _entering letters instead of digits as contact's phone number_. Normally these exceptions should be caught, however as this is a minimal demo project, exceptions are thrown but not caught ( though, API availability exceptions **are** caught as it's part of the problem description ).
* This project is based on **php7**, **symfony** components,and **mysql**(mariaDB) and does not incorporate caching mechanism with memcached.
* The **Report** page (located at route **/report**) provides a log of all messages sent along with their status and respective External SMS API.
* **
## Contact Me
In case you have questions or comments, do not hesitate to contact me at [caffotinated@gmail.com](mailto:caffotinated@gmail.com)